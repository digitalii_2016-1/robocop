//---------------------------------------------------------------------------
// Wishbone UART 
//
// Register Description:
//
//    0x00 UCR      [ 0 | 0 | 0 | tx_busy | 0 | 0 | rx_error | rx_avail ]       Si wb_adr_i termina en 0x00, direcciona el reg UCR
//    0x04 DATA    								Si wb_adr_i termina en 0x04, direcciona el reg DATA
//
//---------------------------------------------------------------------------

module wb_uart #(
	parameter          clk_freq = 50000000,
//	parameter          baud     = 115200
	parameter          baud     = 57600
) (
	input              clk,
	input              reset,
	// Wishbone interface
	input              wb_stb_i,
	input              wb_cyc_i,
	output             wb_ack_o,
	input              wb_we_i,
	input       [31:0] wb_adr_i,
	input        [3:0] wb_sel_i,
	input       [31:0] wb_dat_i,
	output reg  [31:0] wb_dat_o,
	// Serial Wires
	input              uart_rxd,
	output             uart_txd
);

//---------------------------------------------------------------------------
// Actual UART engine
//---------------------------------------------------------------------------
wire [7:0] rx_data;
wire       rx_avail;
wire       rx_error;
reg        rx_ack;
wire [7:0] tx_data;
reg        tx_wr;
wire       tx_busy;

uart #(
	.freq_hz(   clk_freq ),
	.baud(      baud     )
) uart0 (
	.clk(       clk      ),
	.reset(     reset    ),
	//
	.uart_rxd(  uart_rxd ),
	.uart_txd(  uart_txd ),
	//
	.rx_data(   rx_data  ),
	.rx_avail(  rx_avail ),
	.rx_error(  rx_error ),
	.rx_ack(    rx_ack   ),
	.tx_data(   tx_data  ),
	.tx_wr(     tx_wr    ),
	.tx_busy(   tx_busy  )
);

//---------------------------------------------------------------------------
// 
//---------------------------------------------------------------------------
wire [7:0] ucr = { 3'b0, tx_busy, 2'b0, rx_error, rx_avail }; //UART Control Register??

wire wb_rd = wb_stb_i & wb_cyc_i & ~wb_we_i;                   //cuando se puede leer
wire wb_wr = wb_stb_i & wb_cyc_i &  wb_we_i & wb_sel_i[0];     //cuando se puede escribir

reg  ack;

assign wb_ack_o = wb_stb_i & wb_cyc_i & ack; //acknowledge cuando se reciba un strobe, un cycle y ack

assign tx_data = wb_dat_i[7:0];  //solo usa los 8 bits menos significativos del dato entrante.

always @(posedge clk)
begin
	if (reset) begin
		wb_dat_o[31:8] <= 24'b0; //salida hacia WB
		tx_wr  <= 0;             //salida hacia UART.v
		rx_ack <= 0;
		ack    <= 0;
	end else begin
		wb_dat_o[31:8] <= 24'b0; //los 24 bits más significativos del dato de salida siempre son cero
		tx_wr  <= 0;
		rx_ack <= 0;
		ack    <= 0;

		if (wb_rd & ~ack) begin //si lectura habilitada, pero el dato no se ha leido
			ack <= 1;       // lectura recibida
 
                       //todo depende del direccionamiento que se haga, así retornará lo que el LM32 necesite.

			case (wb_adr_i[3:2])             //Solo tiene en cuenta los bits 3 y 4 de la DIRECCIÓN 
                                                         // (mirar el archivo main.c y soc-hw.c del ejemplo boot0_serial)
			2'b00: begin                     //32'bXXXX XXXX XXXX XXXX XXXX XXXX XXXX 00XX : 0xXXXXXXX0-0xXXXXXXX3   
				wb_dat_o[7:0] <= ucr;    // Si se direcciona wb_adr_i[3:2]=2'b00, la salida es el contenido de ucr:
                                                         // { 3'b0, tx_busy, 2'b0, rx_error, rx_avail }
			end
			2'b01: begin                     //32'bXXXX XXXX XXXX XXXX XXXX XXXX XXXX 01XX : 0xXXXXXXX4-0xXXXXXXX7   
				wb_dat_o[7:0] <= rx_data;// Si se direcciona wb_adr_i[3:2]=2'b01, la salida es el contenido de rx_data:
				rx_ack        <= 1;
			end
			default: begin
				wb_dat_o[7:0] <= 8'b0;
			end
			endcase
		end else if (wb_wr & ~ack ) begin
			ack <= 1;

			if ((wb_adr_i[3:2] == 2'b01) && ~tx_busy) begin
				tx_wr <= 1;
			end
		end
	end
end


endmodule
