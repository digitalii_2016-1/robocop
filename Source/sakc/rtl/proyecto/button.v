`timescale 1ns / 1ps

// __________________________________________________________________
// Este mdulo recibe la seal de pulsacin de un nico boton, e implementa 
// el circuito de antirrebote. Al final, entrega la duracin de la presin 
// del boton en # de pulsos de reloj.

module button (
				input clk,
				input rst,
				input button,
				output reg [31:0] timeCount = 0
    );
	 
//Parameters:	 
parameter DELAY_SAMPLING = 500_000;   // conteo de retardo para muestreo de la seal del boton

//Initial Declarations
wire debounced;                                 // debounced button signal 	
reg [18:0] counter = 0;
reg [31:0] counter2 = 0;
reg endCount = 0;                    // flag de finalizacin de conteo
reg delay = 0;
wire timeCountReady;

//******************************************************************//
//**************** DEBOUNCING CIRCUIT: FSM *************************//
//******************************************************************//

//  states transition:
reg current = 0;
reg next = 0;

always @(posedge clk)
begin
	if ( rst )
		current <= 0;
	else
		current <= next;
end

// FSM definition:
always @( * )
begin
	case (current)
		0: if (button)
				next <= 1;
			else
				next <= 0;
		1: if (delay)
				if (button)
					next <= 1;
				else 
					next <= 0;
			else
				next <= 1;
	endcase
end	
		
assign debounced = (current == 1); 		

//delay de 10ms:
always @(posedge clk)
begin
	if ( rst ) begin
		counter <= 0;
		delay <= 0;
	end else begin
		if (current == 1) begin                        // Inicia el retardo si se presiona el boton
			if (counter == DELAY_SAMPLING) begin        // a 50MHz, cuenta hasta 500.000 para generar un retardo de 10ms
				counter <= 0;
				delay <= 1;
			end else begin
				counter <= counter + 1;
				delay <= 0;
			end
		end else begin
			counter <= 0;
			delay <= 0;
		end
	end
end


//******************************************************************//
//******* ESTIMACION DE LA DURACIN DE LA PRESIN DEL BOTON ********//
//******************************************************************//

reg [1:0] state = 0;
reg [1:0] nextState = 0;

// States transitions
always @(posedge clk)
begin
	if ( rst )
		state <= 0;
	else begin
		state <= nextState;
	end
end

// FSM definition
always @( * )
begin
	case (state)
		0: if (debounced)              // Se presion el boton?
				nextState <= 1;
			else 
				nextState <= 0;
		1: if (~debounced) // Se dej de oprimir?
				nextState <= 2;
			else
				nextState <= 1;
		2: if (timeCountReady)
				nextState <= 0;
			else 
				nextState <= 2;
	endcase
end	
			
// Contador de duracin de pulso (cuanto se demora la seal debouned en 1)

always @(posedge clk)
begin
	if ( rst ) begin
		counter2 <= 0;
		endCount <= 0;
		timeCount <= 0;
	end else begin
		if (state == 1) begin     // Cuenta mientras el boton est oprimido.
			if (counter2 >= 28'd150_000_000) begin
				counter2 <= 28'd150_000_000;
				endCount <= 0;
				timeCount <= counter2;
			end else		
				counter2 <= counter2 + 1;
				endCount <= 0;
				timeCount <= counter2;
		end else begin
			counter2 <= 0;
			endCount <= 1;
			timeCount <= timeCount;  // Cuando no se presiona el boton, timeCount conserva el ltimo valor recibido
		end
	end
end

assign timeCountReady = endCount;
		
	


endmodule
