`timescale 1ns / 1ps

//_________________________________________________________________________
// Este modulo recibe como entrada la duraci�n en pulsos de reloj del ancho
// de pulso del PWM que se debe enviar al motor. Con este ancho de pulsos,
// construye la se�al de PWM y la env�a al motor.

module motor (
		// Intputs
				input clk, 
				input rst, 
				input [31:0] PWM_width,
		// Outputs
				output PWM
    );

// ********** DEFINITIONS *********** //
// ********************************** //

//    `define FPGA_20MHz   
      `define FPGA_50MHz

// *********** PARAMETERS *********** // 
// ********************************** // 	

`ifdef FPGA_20MHz
       parameter   FPGA_FREQ  =  20_000_000; // 20MHz
`endif
`ifdef FPGA_50MHz
		 parameter   FPGA_FREQ  =  50_000_000; // 20MHz
`endif
       parameter   PWM_FREQ   =  50;         // 50Hz --> 20ms period

		 parameter PWM_COUNT = FPGA_FREQ / PWM_FREQ;  // # counts to get PWM period
		 parameter NEUTRAL_TIME = 667;             // NEUTRAL_TIME = 1 / NEUTRAL_POSITION : 1/1,5 ms

		 parameter NEUTRAL_COUNT = FPGA_FREQ / NEUTRAL_TIME;


// ************* CLOCK ADJUSTMENT **************** //
// *********************************************** //

// 50 Hz Clock tick generation: 20ms period
reg [19:0] i = 0;

always @(posedge clk) 
begin	
	if ( rst )
		i <= 20'd0;
	else begin
		if (i < PWM_COUNT)
			i <= i + 1;
		else
			i <= 20'd0;
	end
end

wire f50Hz_tick = (i == 20'd0);  // tick every 20ms

// ******** PULSE WIDTH SELECTION ********** //
// ***************************************** //

reg [31:0] PWM_width_reg = 0;

// El registro del ancho de pulso siempre est� actualizandose con lo que le llegue a la entrada:
always @(posedge clk) 
begin
	if ( rst ) 
		PWM_width_reg <= NEUTRAL_COUNT;
	else if (PWM_width == 0) 
		PWM_width_reg <= NEUTRAL_COUNT;
	else 
		PWM_width_reg <= PWM_width;
end
				
// ******* FINITE STATE MACHINE ********* //
// ************************************** //

reg finish_pulse = 0;
reg [16:0] j = 0;
reg next_state = 0;  // NOT a phisical reg, remembre!!!: all variable inside always @ structure, must be declared as "reg"
reg state = 0;

always @(posedge clk)
begin
	if ( rst ) 
		state <= 0;
	else
		state <= next_state;
end

always @( * )
begin 
	case (state)
		0: if (f50Hz_tick)
				next_state <= 1;
			else 
				next_state <= 0;
		1: if (finish_pulse)
				next_state <= 0;
			else 
				next_state <= 1;
	endcase
end

always @(posedge clk)
begin
	if (state)
		if (j < PWM_width_reg) begin
			j <= j+1;
			finish_pulse <= 0;
		end else begin
			j <= 0;
			finish_pulse <= 1;
		end
	else begin
		finish_pulse <= 0;
		j <= 0;
	end
end

assign PWM = (state == 1);	


endmodule
