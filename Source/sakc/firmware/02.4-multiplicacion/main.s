	.file	"main.c"
	.global	__mulsi3
	.section	.text
	.align 4
	.global	main
	.type	main, @function
main:
	addi     sp, sp, -28
	sw       (sp+8), fp
	sw       (sp+4), ra
	addi     fp, r0, 28
	add      fp, fp, sp
	sw       (fp+-12), r1
	sw       (fp+-16), r2
	addi     r1, r0, 10
	sw       (fp+0), r1
	addi     r1, r0, 13
	sw       (fp+-4), r1
	addi     r1, r0, 0
	sw       (fp+-8), r1
	lw       r1, (fp+0)
	lw       r2, (fp+-4)
	calli    __mulsi3
	sw       (fp+-8), r1
	lw       r1, (fp+-8)
	lw       fp, (sp+8)
	lw       ra, (sp+4)
	addi     sp, sp, 28
	b        ra
	.size	main, .-main
	.ident	"GCC: (GNU) 4.5.2 20101208 (prerelease)"
