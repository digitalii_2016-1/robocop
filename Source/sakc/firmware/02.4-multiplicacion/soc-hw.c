#include "soc-hw.h"

uart_t   *uart0  = (uart_t *)   0xF0000000; // puntero de tipo struct, a la estructura uart_t: posee dos campos: ucr & rxtx
timer_t  *timer0 = (timer_t *)  0xF0010000;
gpio_t   *gpio0  = (gpio_t *)   0xF0020000;
// uint32_t *sram0  = (uint32_t *) 0x40000000;

uint32_t msec = 0;

/***************************************************************************
 * General utility functions
 */
void sleep(int msec)
{
	uint32_t tcr;

	// Use timer0.1
	timer0->compare1 = (FCPU/1000)*msec;
	timer0->counter1 = 0;
	timer0->tcr1 = TIMER_EN | TIMER_IRQEN;

	do {
		//halt();
 		tcr = timer0->tcr1;
 	} while ( ! (tcr & TIMER_TRIG) );
}

void tic_init()
{
	// Setup timer0.0
	timer0->compare0 = (FCPU/1000);
	timer0->counter0 = 0;
	timer0->tcr0     = TIMER_EN | TIMER_AR | TIMER_IRQEN;
}

/***************************************************************************
 * UART Functions
 */
void uart_init()
{
	//uart0->ier = 0x00;  // Interrupt Enable Register
	//uart0->lcr = 0x03;  // Line Control Register:    8N1
	//uart0->mcr = 0x00;  // Modem Control Register

	// Setup Divisor register (Fclk / Baud)
	//uart0->div = (FCPU/(57600*16));
}

char uart_getchar()
{   
	while (! (uart0->ucr & UART_DR)) ; // UART_DR: 8 bits (0x01) RX Data ready : si lo que contiene el reg de control ucr != 0x01 ...(mientras la recepción no 				    //	                                                                                                                   esté completa...)
	return uart0->rxtx; // uart0 es un campo del puntero (ver def arriba). "->" es apuntar a la direccion almacenada en el puntero. 
			    // rxtx es un campo de tipo volatile de la estructura uart_t, definida en el soc-hw.c (32 bits) 
}			    // esta función retorna el contenido del campo rxtx.
			   
			    // OJO: a nivel hw, rxtx & ucr son registros de la estructura uart_t. (ucr: reg de control)
void uart_putchar(char c)
{
	while (uart0->ucr & UART_BUSY) ; // UART_BUSY = 0x10  TxBusy : si lo que contiene el reg de control ucr es = 0x01 ... (si la UART esta ocupada en envíos)
	uart0->rxtx = c;                 // en el reg rxtx, ponga c
}

void uart_putstr(char *str)  // *str apunta al primer caracter de la cadena recibida 
{
	char *c = str;
	while(*c) {
		uart_putchar(*c); // realiza una carga caracter a caracter:
		c++;		  // se desplaza desde el primer caracter hasta el último recibido
	}
}
int uart_getint() //funcion creada por nosotros. no se ha probado.
{
	while (! (uart0->ucr & UART_DR)) ;
	return uart0->rxtx;

}

