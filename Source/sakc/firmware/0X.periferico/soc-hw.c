#include "soc-hw.h"

uart_t   *uart0  = (uart_t *)   0xF0000000; //uart_t: estructura, tiene como campos: ucr, rxtx, ambos son uint32_t.
					    // *uart0: inicializa el puntero con 0xF0000000 ???
                                            // el direccionamiento hacia la UART empieza en 0xF0000000 ???: entonces el puntero está apuntando
                                            // hacia donde se encuentran los registros de comunicación con la UART.
timer_t  *timer0 = (timer_t *)  0xF0010000; // y hacia el timer, en 0xF0010000 ??
gpio_t   *gpio0  = (gpio_t *)   0xF0020000; // y hacia el gpio, en 0xF0020000 ??
// uint32_t *sram0  = (uint32_t *) 0x40000000;

uint32_t msec = 0;

/***************************************************************************
 * General utility functions
 */
void sleep(int msec)
{
	uint32_t tcr;

	// Use timer0.1
	timer0->compare1 = (FCPU/1000)*msec;
	timer0->counter1 = 0;
	timer0->tcr1 = TIMER_EN | TIMER_IRQEN;

	do {
		//halt();
 		tcr = timer0->tcr1;
 	} while ( ! (tcr & TIMER_TRIG) );
}

void tic_init()
{
	// Setup timer0.0
	timer0->compare0 = (FCPU/1000);
	timer0->counter0 = 0;
	timer0->tcr0     = TIMER_EN | TIMER_AR | TIMER_IRQEN;
}

/***************************************************************************
 * UART Functions
 */
void uart_init()
{
	//uart0->ier = 0x00;  // Interrupt Enable Register
	//uart0->lcr = 0x03;  // Line Control Register:    8N1
	//uart0->mcr = 0x00;  // Modem Control Register

	// Setup Divisor register (Fclk / Baud)
	//uart0->div = (FCPU/(57600*16));
}

char uart_getchar()
{   
	while (! (uart0->ucr & UART_DR)) ; //UART_DR=0x01: RX Data Ready : 
                             /*  Lo que dice es:  
                                 Mientras lo que haya en ucr ({ 3'b0, tx_busy, 2'b0, rx_error, rx_avail }) NO coincida (!) con 0x01, 
                                 es decir, no exista aún un dato recibido, que el LM32 continúe esperando (se quede en el while) hasta que la
                                 bandera rx_avail se ponga en 1, es decir, que exista un dato disponible para ser recibido. Inmediatamente, 
                                 que pase el dato recibido al registro rxtx
                             */
	return uart0->rxtx; //uart0 es un puntero. "->" es apuntar a la direccion almacenada en el puntero. Equivalente a uart0 = &rxtx ??
			    //devuelve lo que esté en la dirección de rxtx
       //Como hace para que uart0->rxtx apunte a 0xF00000004??? esto es lo que creo: Al definir la estructura en el soc-hc.h, ucr se definió
       //  como el primer campo y rxtx como el segundo; ambos de 32 bits de largo. Al declarar uart_t   *uart0  = (uart_t *)   0xF0000000 al
       // inicio de este archivo, ucr y rxtx son APUNTADORES A DATOS DE 32 BITS, así, ucr le correspondería apuntar al dato que va desde
       // 0xF0000000 hasta 0xF0000003 y rxtx le correspondería apuntar al dato que va desde 0xF0000004 hasta 0xF0000007. (la memoria se
       // direcciona de a bytes). Puesto que el LM32 es little endian, para referirse al dato de ucr, solo es necesario apuntar a la sección
       // 0xF0000000 para reconocer todo el dato. De igual manera con rxtx.
}

void uart_putchar(char c) //para enviar (transmitir) caracteres hacia el PC
{
	while (uart0->ucr & UART_BUSY) ; //mientras la UART esté ocupada enviando un caracter, quedese en el while.
	uart0->rxtx = c;                 //cuando se desocupe, se envía al registro rxtx el siguiente caracter
}

void uart_putstr(char *str) 
{
	char *c = str;
	while(*c) {      //mientras el contenido de c no sea cero
		uart_putchar(*c); // realiza una carga caracter a caracter
		c++; //char es 1 byte, y la memoria se direcciona de a bytes. c++ direcciona al siguiente byte, siguiente caracter.
	}
}
int uart_getint() //funcion creada por nosotros. no se ha probado.
{
	while (! (uart0->ucr & UART_DR)) ;
	return uart0->rxtx;

}

/***************************************************************************
 * GPIO PREDEFINED FUNCTIONS
 */

int gpio_get_in(){
	
	return gpio0->in;
}

int gpio_get_out(){
	
	return gpio0->out;
}

int gpio_get_oe(){
	
	return gpio0->oe;
}

void gpio_put_out(int output){

	gpio0->out = output;            
}

void gpio_put_oe(int output){

	gpio0->oe = output;            
}
















