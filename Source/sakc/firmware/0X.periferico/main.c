
#include "soc-hw.h"

int main(void){

	while(1){

		volatile unsigned int switches;

		switches = gpio_get_in();

		gpio_put_out(switches);
	}

	return 0;

}

