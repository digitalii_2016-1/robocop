/**
 * Primitive first stage bootloader 
 *
 *
 */
#include "soc-hw.h"
#include "math.h"

/* prototypes */
uint32_t read_uint32()
{
	uint32_t val = 0, i;

    for (i = 0; i < 4; i++) {
        val <<= 8; //asignación con desplazamiento a la izquierda
        val += (uint8_t)uart_getchar();  //asignación con suma.
    }

    return val;
}

int main(int argc, char **argv)
{
	int8_t  *p; //char con signo (puntero)//lo de char solo refiere al tamaño de variable
	uint8_t  c; //char sin signo

	// Initialize UART
	uart_init();

	c = '*'; // print msg on first iteration
	for(;;) {
		uint32_t start, size; 

		switch (c) {
    		case 'u': // upload 
    			start = read_uint32();
    			size  = read_uint32();
    			for (p = (int8_t *) start; p < (int8_t *) (start+size); p++)
    				*p = uart_getchar();
    			break;
		case 'd': // download
    			start = read_uint32();
    			size  = read_uint32();
    			for (p = (int8_t *) start; p < (int8_t *) (start+size); p++)
				uart_putchar( *p );
    			break;
    		case 'g': // goto
    			start = read_uint32();
    			jump(start);
    			break;   
		default:
			uart_putstr("**SAKC/bootloader** > \r\n"); //el argumento de uart_putstr es un apuntador de tipo char.
								// un apuntador puede ser inicializado así: char *cadena=esto es una cadena
			break;
		};
		c = uart_getchar();
	}
}

//en C, las direcciones de memoria se identifican con el ampersand: &dato: dirección de la variable dato.

