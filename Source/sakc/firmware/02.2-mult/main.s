	.file	"main.c"
	.section	.rodata
	.align 4
.LC0:
	.string	"Ingrese el numero uno \r\n"
	.align 4
.LC1:
	.string	"Ingrese el numero dos \r\n"
	.global	__mulsi3
	.align 4
.LC2:
	.string	"El resultado es \r\n"
	.section	.text
	.align 4
	.global	main
	.type	main, @function
main:
	addi     sp, sp, -32
	sw       (sp+8), fp
	sw       (sp+4), ra
	addi     fp, r0, 32
	add      fp, fp, sp
	sw       (fp+-16), r1
	sw       (fp+-20), r2
	addi     r1, r0, 0
	sw       (fp+0), r1
	orhi     r1, r0, hi(.LC0)
	ori      r1, r1, lo(.LC0)
	calli    uart_putstr
	calli    uart_getint
	sw       (fp+-4), r1
	orhi     r1, r0, hi(.LC1)
	ori      r1, r1, lo(.LC1)
	calli    uart_putstr
	calli    uart_getint
	sw       (fp+-8), r1
	lw       r1, (fp+-4)
	addi     r1, r1, -48
	sw       (fp+-4), r1
	lw       r1, (fp+-8)
	addi     r1, r1, -48
	sw       (fp+-8), r1
	lw       r1, (fp+-4)
	lw       r2, (fp+-8)
	calli    __mulsi3
	sw       (fp+0), r1
	lw       r1, (fp+0)
	addi     r1, r1, 48
	sw       (fp+0), r1
	lw       r1, (fp+0)
	sb       (fp+-9), r1
	orhi     r1, r0, hi(.LC2)
	ori      r1, r1, lo(.LC2)
	calli    uart_putstr
	lbu      r1, (fp+-9)
	calli    uart_putchar
	lw       fp, (sp+8)
	lw       ra, (sp+4)
	addi     sp, sp, 32
	b        ra
	.size	main, .-main
	.ident	"GCC: (GNU) 4.5.2 20101208 (prerelease)"
