
#include "soc-hw.h"
//#include <math.h>

// Estas son las 3 funciones usadas para este código, definidas en el soc-hw.c
//   int get_button1A()
//   int get_button1B()
//   void send_width(int output)

extern int get_button1A();
extern int get_button1B();
extern void send_width(int output);

int main(void){

	volatile int left;
	volatile int right;
	volatile int total;
	volatile int pwm_width;  //contiene el No de cuentas de reloj para generar el PWM
	volatile int a;

while (1) {
	right = get_button1A();
	left  = get_button1B();

	total = right - left;

	total = total >> 13;
	total = total*total;
	total = total >> 13;
	
	if ( total > 40924 ){
		pwm_width = 40924 + 75000;
	}
	else if ( total >= 0) {
		pwm_width = total + 75000;
	}
	else if ( total < 0) {
		pwm_width = -total+75000;
	}
	else {
		pwm_width = 0;
	}
	
	send_width(pwm_width);
}


//CODIGO DE PRUEBA TAN SOLO ENVIANDO EL ANCHO DE PULSO. FUNCIONÓ!	
/*
	volatile int pwm_width;
	volatile int i;

while (1){

	for (i=0;i<10000000;i++){
		pwm_width=100000; 	
		send_width(pwm_width);	
		i=i+1;
		}
	for (i=0;i<10000000;i++){
		pwm_width=50000; 	
		send_width(pwm_width);	
		i=i+1;
		}
	}
*/
	return 0;

}










