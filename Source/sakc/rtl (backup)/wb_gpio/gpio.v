
// Las salidas I/O del wb_gpio.v son las entradas a este módulo:

//       WB_GPIO        |    GPIO

//       gpio_in <---------- out
//       gpio_out ---------> in
//       gpio_oe ----------> in2


module gpio (
	input        [7:0] switches,
	output       [7:0] leds,
	// wb_gpio interface
	input       [31:0] in,
	input       [31:0] in2,
	output      [31:0] out
);

/*
assign out = { 24'd0, switches };

assign leds = in[7:0];
*/

assign leds = switches;

endmodule
