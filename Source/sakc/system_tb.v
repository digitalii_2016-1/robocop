//----------------------------------------------------------------------------
//
//----------------------------------------------------------------------------
`timescale 1 ns / 100 ps

module system_tb;

//----------------------------------------------------------------------------
// Parameter (may differ for physical synthesis)
//----------------------------------------------------------------------------
parameter tck              = 20;       // clock period in ns
parameter uart_baud_rate   = 1152000;  // uart baud rate for simulation 

parameter clk_freq = 1000000000 / tck; // Frequenzy in HZ = 50MHz
//----------------------------------------------------------------------------
//
//----------------------------------------------------------------------------
reg        clk;
reg        rst;
wire       led;

//----------------------------------------------------------------------------
// UART STUFF (testbench uart, simulating a comm. partner)
//----------------------------------------------------------------------------

//wire         uart_rxd;
reg 	     uart_rxd;
wire         uart_txd;
//----------------------------------------------------------------------------

// Device Under Test 
//----------------------------------------------------------------------------
system #(
	.clk_freq(           clk_freq         ),
	.uart_baud_rate(     uart_baud_rate   )
) dut  (
	.clk(          clk    ),
	// Debug
	.rst(          rst    ),
	.led(          led    ),
	// Uart
	.uart_rxd(  uart_rxd  ),
	.uart_txd(  uart_txd  )
);

/* Clocking device */
initial         clk <= 0;
always #(tck/2) clk <= ~clk;

/* Simulation setup */
initial begin

	uart_rxd <= 1;

	$dumpfile("system_tb.vcd");
	//$monitor("%b,%b,%b,%b",clk,rst,uart_txd,uart_rxd);
	$dumpvars(-1, dut);
	//$dumpvars(-1,clk,rst,uart_txd);
	// reset
	#0  rst <= 1;
	#80 rst <= 0;

	#(tck*2500) $finish;
end


always begin

#170000;
// envío del char "2" = 0x32
uart_rxd <= 0; #640; //start bit
uart_rxd <= 0; #640; //LSB
uart_rxd <= 1; #640;
uart_rxd <= 0; #640;
uart_rxd <= 0; #640;
uart_rxd <= 1; #640;
uart_rxd <= 1; #640;
uart_rxd <= 0; #640;
uart_rxd <= 0; #640; //MSB
uart_rxd <= 1; #840; //bit de fin
#170000;
// envío del char "3" = 0x33
#170000;
uart_rxd <= 0; #640; //start bit
uart_rxd <= 1; #640; 
uart_rxd <= 1; #640;
uart_rxd <= 0; #640;
uart_rxd <= 0; #640;
uart_rxd <= 1; #640;
uart_rxd <= 1; #640;
uart_rxd <= 0; #640;
uart_rxd <= 0; #640;
uart_rxd <= 1; #840; //bit de fin
#170000; 

end

endmodule
