//Archivos especificos de ChibiOs
#include "ch.h"
#include "hal.h"


#include "test.h"
#include "chprintf.h"
#include "atmel_adc.h"
#include "pio.h"
#include "pmc.h"
#include "psram.h"
#include "board.h"

// Crea un macro del area de trabajo y el espacio en la pila del hilo
static WORKING_AREA(waThread1, 128);//Definición del área de trabajp
static msg_t Thread1(void *arg) { //Definicion del Thread1
  (void)arg;
  while (TRUE) {
    palClearPad(IOPORT3, 17);// LED conectado a PC17, 
    chThdSleepMilliseconds(500);
    palSetPad(IOPORT3, 17);
    chThdSleepMilliseconds(500);
  }
  return(0);
}

/*
 * Application entry point.
 */
int main(void) {
    uint32_t i,j;
    uint32_t *ptr = (uint32_t *) PSRAM_BASE_ADDRESS; // 0x63000000  NCS3. Puntero que apunta a 0x63..

    halInit();
    chSysInit();
    sdStart(&SD1, NULL);  /* Activates the serial driver 2 */ 
    /* Creates the blinker thread. */
	
/* PARAMETROS chTchThdCreateStatic
(puntero al area de trabajo para ese hilo,tamaño del área del trabajo, nivel de prioridad del hilo, función de hilo, argumento que se a la función del hilo)*/
    chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL); //Crea e inicia el Thread1


    /* Configure EBI I/O for psram connection*/
/*Parametros PIO_Configure controldor paralelo entrada y salida, asigna la lista de pines al periférico EBI
(apuntador a instancia PIO psram.h,tipo de PIO, mascara de Bits de los pines a configurar, atributos de los pines*/
    PIO_Configure(pinPsram, PIO_LISTSIZE(pinPsram));

    /* complete SMC configuration between PSRAM and SMC waveforms. Static Memory Control*/ 
    BOARD_ConfigurePSRAM( SMC ) ;

  while (TRUE) {

    for (i = 0; i < 256; ++i) {
            ptr[i] =  i;
    }

    for (j=0; j< 16; j++){
      for (i = 0; i < 16; ++i) {
        chprintf((BaseChannel *)&SD1, ",%x \t", ptr[j*16 + i] & 0xFF) ;
      }
      chprintf((BaseChannel *)&SD1, "\n\r" ) ;
    }

    chThdSleepMilliseconds(500);

  }

  return(0);
}
