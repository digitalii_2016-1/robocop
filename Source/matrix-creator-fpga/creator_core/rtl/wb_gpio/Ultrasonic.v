`timescale 1ns / 1ps

//___________________________________________________________________________
// El modulo tiene como entrada el reloj de 25MHz de la FPGA en base a esto
// se crea un reloj de 10uS para el Trigger y finalmente se envia por el SPI
// la salida del sistema.

module ultrasonic (
		// Intputs
				input clk, //La frecuencia de ingreso en el system.v es de 25MHz
				input rst, 
				input Echo,
		// Outputs
				output reg [15:0] send_data,
				output Trigger
    );

parameter FPGA_FREQ = 25_000_000;   // 25 MHz la frecuencia de la FPGA
parameter TRIGGER_FREQ = 10; 		// 10 Hz - 100ms
parameter TRIGGER_WIDTH = 500; 		// Valor para que sea de tamaño 20uS (Minimo 10uS según el datasheet)

parameter ECHO_FREQ = 1_000_000;	// 1us para poder contar cuantas veces se suma el Echo antes de que llegue la senal
parameter US_COUNT = (FPGA_FREQ / ECHO_FREQ) -1;

pwm_mod pwm(
	.clk(clk),
	.rst(rst),
	.PWM_FREQ(TRIGGER_FREQ),
	.PWM_width(TRIGGER_WIDTH),
	.PWM(Trigger)
);

reg clk_us = 0;
reg [4:0] counter_us = 0;
reg [29:0] counter_echo = 0;
reg read = 0;
reg f_time = 0;
reg [15:0] counter_out=0;

always @(posedge clk) begin
	// Clock Microsegundos para contar los centimetros	
	if(rst) counter_us<=0;
	else begin
		if(counter_us == US_COUNT) begin
			counter_us <= 0;
			clk_us <= 1;
		end else begin
			counter_us <= counter_us +1;
			clk_us <=0;
		end
	end
end

always @(posedge clk_us) begin		// Cuenta la cantidad de pulsos arriba del Echo para saber la distancia en cm
	if(Echo) begin
		f_time <=1;
		counter_echo <= counter_echo +1;
	end else begin
		if(f_time) begin	
			send_data <= counter_echo;			
			f_time<=0;
		end 
		counter_echo <=0;
	end
end

endmodule 
