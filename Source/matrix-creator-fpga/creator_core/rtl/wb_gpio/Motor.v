`timescale 1ns / 1ps

//_________________________________________________________________________
// Este modulo recibe como entrada la duración en pulsos de reloj del ancho
// de pulso del PWM que se debe enviar al motor. Con este ancho de pulsos,
// construye la señal de PWM y la envía al motor.

module motor (
		// Intputs
				input clk, //La frecuencia de ingreso en el system.v es de 25MHz
				input rst,
				input [15:0] PWM_width,
				input [15:0] PWM2_width,
				input [15:0] giro,
		// Outputs
				output 			PWM_motor1,
				output 			PWM_motor2,
				//output reg GIRO
				output 	reg		Giro0_motor1,
				output 	reg		Giro1_motor1,
				output 	reg		Giro0_motor2,
				output 	reg		Giro1_motor2
				
    );

// Variable de frecuencia del motor 
parameter MOTOR_FREQ = 400;

pwm_mod pwm(
	.clk(clk),
	.rst(rst),
	.PWM_FREQ(MOTOR_FREQ),
	.PWM_width(PWM_width),
	.PWM(PWM_motor1)
);

pwm_mod pwm2(
	.clk(clk),
	.rst(rst),
	.PWM_FREQ(MOTOR_FREQ),
	.PWM_width(PWM2_width),
	.PWM(PWM_motor2)
);
always @(posedge clk)
	begin
	
		case (giro[1:0])
				2'b01: begin
				Giro0_motor1=1;
				Giro1_motor1=0;
				end
				2'b10: begin
				Giro0_motor1=0;
				Giro1_motor1=1;
				end
				default: begin
				Giro0_motor1=0;
				Giro1_motor1=0;
				end
			endcase
		case (giro[3:2])
				2'b01: begin
				Giro0_motor2=1;
				Giro1_motor2=0;
				end
				2'b10: begin
				Giro0_motor2=0;
				Giro1_motor2=1;
				end
				default: begin
				Giro0_motor2=0;
				Giro1_motor2=0;
				end
		endcase

	end


endmodule
