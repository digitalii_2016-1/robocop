//---------------------------------------------------------------------------
// Wishbone General Pupose IO Component
//
//     0x00	
//     0x10     gpio_in    (read-only)
//     0x14     gpio_out   (read/write)
//     0x18     gpio_oe    (read/write)
//
//---------------------------------------------------------------------------

module wb_gpio (
	input 				clk,
	input 				reset,
	input 				Echo,
	input 				pulsador1,
	input 				pulsador2,
		output 			Trigger,	
		output 			PWM_motor1,
		output 			PWM_motor2,
		output 			Giro0_motor1,
		output 			Giro1_motor1,
		output 			Giro0_motor2,
		output 			Giro1_motor2,
		
		///output [15:0]		counter_out,

	// Wishbone interface
	input				wb_stb_i,
	input               wb_cyc_i,
	input               wb_we_i,
	input		[13:0]  wb_adr_i,
	input		[3:0]   wb_sel_i,
	input		[15:0]  wb_dat_i,
	output reg	[15:0]  wb_dat_o
);

//---------------------------------------------------------------------------
// 
//---------------------------------------------------------------------------

wire [15:0] gpiocr = 15'b0;


//los pines de entrada/Salida del GPIO son ahora wires:
wire [15:0] ultrasonic_data;
reg [15:0] pulsadores=16'b0;

reg [15:0] pwm1 = 16'd12500;
reg [15:0] pwm2 = 16'd12500;
reg [15:0] giro = 16'b0;


wire wb_rd = wb_stb_i & wb_cyc_i & ~wb_we_i;
wire wb_wr = wb_stb_i & wb_cyc_i &  wb_we_i;

always @(posedge clk) begin
	if (reset) begin
		//ack      <= 1'b0;
		//gpio_out <= 'b0;
	end else begin
		if (wb_rd) begin           // read cycle
			case (wb_adr_i[1:0])
				2'b00: wb_dat_o <= gpiocr;
				2'b01: wb_dat_o <= ultrasonic_data;
				2'b11: wb_dat_o <= pulsadores;
				default: wb_dat_o <= 16'b0;
			endcase
		end else if (wb_wr) begin // write cycle
			//ack <= 1'b1;

			case (wb_adr_i[1:0])
				2'b01: pwm1 <= wb_dat_i;
				2'b10: giro <= wb_dat_i;
				2'b11: pwm2 <= wb_dat_i;
				default: wb_dat_o <= 16'b0;
			endcase
		end
	end
end

wire [15:0] giro_motores=giro;

motor motor1(
	//Entradas
	.clk(clk),
	.rst(reset),
	.PWM_width(pwm1),  // conteo del ancho de pulso calculado por el procesador
	.giro(giro_motores),
	.PWM2_width(pwm2),  
	//Salidas
	.PWM_motor1( PWM_motor1	),   // Salida en PWM hacia el motor 1
	.PWM_motor2( PWM_motor2	),   // Salida en PWM hacia el motor 1
	.Giro0_motor1( Giro0_motor1 ),
	.Giro1_motor1( Giro1_motor1 ),
	.Giro0_motor2( Giro0_motor2 ),
	.Giro1_motor2( Giro1_motor2 )

);

ultrasonic ultra(
	// Inputs
	.clk(clk),
	.rst(reset),
	.Echo(Echo),
	// Outputs
	.send_data(ultrasonic_data),
	.Trigger(Trigger)
);

always @(posedge clk)
	begin
		pulsadores={pulsador2,pulsador1};			
	end


endmodule
